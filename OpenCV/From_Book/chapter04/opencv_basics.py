import cv2

img = cv2.imread('4.2.03.tiff', cv2.IMREAD_COLOR)

# cv2.namedWindow('Lena', cv2.WINDOW_AUTOSIZE)
cv2.imshow('Mandrill', img)
keyPress = cv2.waitKey(0)
if keyPress == ord('q'):
	cv2.destroyWindow('Mandrill')
elif keyPress == ord('s'):
	cv2.imwrite('test.jpg', img)
	cv2.destroyWindow('Mandrill')