import cv2
import matplotlib.pyplot as plt

img = cv2.imread('4.2.03.tiff', 1)
plt.imshow(img)
plt.title('Mandrill')
plt.axis('off')
plt.show()