import cv2
import matplotlib.pyplot as plt

dataset_path = '/media/rpi4rnd/SANDISK64G/RnD/focusing-computer-vision/OpenCV/Learning by Book/dataset/misc/'

img = cv2.imread(dataset_path + '4.2.07.tiff', 1)
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
plt.imshow(img)
plt.title('COLOR IMAGE')
plt.axis('off')
plt.show()