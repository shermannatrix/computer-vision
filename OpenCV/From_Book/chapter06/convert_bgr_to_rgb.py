import cv2
import matplotlib.pyplot as plt

dataset_path = '/media/rpi4rnd/SANDISK64G/RnD/focusing-computer-vision/OpenCV/Learning by Book/dataset/misc/'

img = cv2.imread(dataset_path + '4.2.07.tiff', 1)
b, g, r = cv2.split(img)
img = cv2.merge((r, g, b))
plt.imshow(img)
plt.title('COLOR IMAGE')
plt.axis('off')
plt.show()