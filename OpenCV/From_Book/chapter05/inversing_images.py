import cv2
dataset_path = '/media/rpi4rnd/SANDISK64G/RnD/focusing-computer-vision/OpenCV/Learning by Book/dataset/misc/'
img = cv2.imread(dataset_path + '4.2.07.tiff', 0)
negative = abs(255 - img)
cv2.imshow('Grayscale', img)
cv2.imshow('Negative', negative)
cv2.waitKey(0)
cv2.destroyAllWindows()