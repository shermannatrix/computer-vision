import cv2
dataset_path = '/media/rpi4rnd/SANDISK64G/RnD/focusing-computer-vision/OpenCV/Learning by Book/dataset/misc/'
img = cv2.imread(dataset_path + '4.1.01.tiff', 1)
b1 = cv2.copyMakeBorder(img, 10, 10, 10, 10, cv2.BORDER_WRAP)
b2 = cv2.copyMakeBorder(img, 10, 10, 10, 10, cv2.BORDER_CONSTANT, value=[255, 0, 0])
cv2.imshow('Wrap', b1)
cv2.imshow('Constant', b2)
cv2.waitKey(0)
cv2.destroyAllWindows()