import cv2
dataset_path = '/media/rpi4rnd/SANDISK64G/RnD/focusing-computer-vision/OpenCV/Learning by Book/dataset/misc/'
img1 = cv2.imread(dataset_path + '4.2.03.tiff', 1)
img2 = cv2.imread(dataset_path + '4.2.05.tiff', 1)
cv2.imshow('NumPy Addition', img1 + img2)
cv2.imshow('OpenMV Addition', cv2.add(img1, img2))
cv2.waitKey(0)
cv2.destroyAllWindows()